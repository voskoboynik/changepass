require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-changepass"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.description  = <<-DESC
                  react-native-changepass
                   DESC
  s.homepage     = "https://github.com/github_account/react-native-changepass"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.authors      = { "Valentin Don" => "valentin.ftp@gmail.com" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://github.com/github_account/react-native-changepass.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true
  # s.resources = "Resources/ChangePassResource.bundle/"
  # s.resource_bundles = {
  #  'ChangePassResource' => [
  #      'Pod/**/*.xib'
  #  ]
  # }

  s.dependency "React"
  # ...
  # s.dependency "..."
end


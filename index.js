import React, { Component } from 'react';
import { requireNativeComponent } from 'react-native';
import PropTypes from 'prop-types'

const ChangePass = requireNativeComponent('ChangePass', null);
ChangePass.propTypes = {
    /**
     * Callback that is called continuously when the user is dragging the map.
     */
    onPasswordChanged: PropTypes.func,
    oldPassword: PropTypes.string,
  };

class ChangePassView extends React.Component  {
    _onPasswordChanged = (event) => {
       // if (!this.props.onPasswordChanged) {
        //   return;
        // }
    
        // // process raw event...
        // this.props.onPasswordChanged(event.nativeEvent);
        console.log("_onPasswordChanged: " + event)
        // alert(Object.keys(event.nativeEvent))
        // alert(Object.values(event.nativeEvent.key))
        alert(event.nativeEvent.key)
        // Object.entries(event).forEach(([key, value]) => {
        //    console.log(key, value);
        // });
      }
      render() {
        return (
          <ChangePass
            {...this.props}
            onPasswordChanged={this._onPasswordChanged}
          />
        );
      }
}

export default ChangePassView;

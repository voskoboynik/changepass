//
//  ChangePassView.h
//  Testlib1
//
//  Created by vdonloc on 18.12.2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTComponent.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChangePassView : UIView
@property (nonatomic, copy) RCTBubblingEventBlock onPasswordChanged;
@property (nonatomic, weak) IBOutlet UITextField *oldPassField;
@property (nonatomic, weak) IBOutlet UITextField *latestPassField;
@property (nonatomic, weak) IBOutlet UILabel *errorLabel;
@property (nonatomic, weak) IBOutlet UIButton *button;
@property (nonatomic, copy) NSString *oldPassword;

@end


@interface UIView (Extention)
+ (UIView*) makeViewFromXib:(NSString*)xibName;
@end


NS_ASSUME_NONNULL_END


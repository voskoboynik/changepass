#import "ChangePassViewManager.h"
#import "ChangePassView.h"
@implementation ChangePassViewManager

RCT_EXPORT_MODULE(ChangePass)


- (UIView *)view
{
    // TODO: Implement some actually useful functionality
//    UILabel * label = [[UILabel alloc] init];
//    [label setTextColor:[UIColor redColor]];
//    [label setText: @"*****"];
//    [label sizeToFit];
//    UIView * wrapper = [[UIView alloc] init];
//
//    [wrapper addSubview:label];
    
    ChangePassView * psView = (ChangePassView*) [ChangePassView makeViewFromXib:@"ChangePassView"];
//    psView.translatesAutoresizingMaskIntoConstraints = NO;
    NSLog(@"psView: %@", psView);
   
    return psView;
}

RCT_EXPORT_VIEW_PROPERTY(oldPassword, NSString)
RCT_EXPORT_VIEW_PROPERTY(onPasswordChanged, RCTBubblingEventBlock)
@end

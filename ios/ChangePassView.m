//
//  ChangePassView.m
//  Testlib1
//
//  Created by vdonloc on 18.12.2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "ChangePassView.h"
@interface ChangePassView()<UITextFieldDelegate>
@end
@implementation ChangePassView
@synthesize oldPassword = _oldPassword;
- (void)awakeFromNib {
    [super awakeFromNib];
//    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.oldPassword = @"";
    [self configureUI];
}

- (void) configureUI {
    self.oldPassField.placeholder = @"Enter old password";
    self.latestPassField.placeholder = @"Enter new password";
    [self.button setTitle:@"Change password" forState:UIControlStateNormal];
    [self.button addTarget:self action:@selector(onTap:) forControlEvents:UIControlEventTouchUpInside];
    self.oldPassField.delegate = self;
}

- (void) onTap:(UIButton*)sender {
    if(self.oldPassField.text == nil || [self.oldPassField.text isEqualToString: @""]) {
        self.errorLabel.text = @"Enter Old password";
        return;
    }
    if(self.latestPassField.text == nil ||  [self.latestPassField.text isEqualToString: @""]) {
        self.errorLabel.text = @"Enter New password";
        return;
    }
    
    if([self.oldPassField.text isEqualToString:self.oldPassword]) {
        [self.errorLabel setHidden: YES];
        NSDictionary * result = @{@"key": self.latestPassField.text};
        if (self.onPasswordChanged) {
            self.onPasswordChanged(result);
            NSLog(@"result: %@", result);
        }
    }else {
        self.errorLabel.text = @"Wrong password";
    }
    
}
- (void) setOldPassword:(NSString *)oldPassword {
    _oldPassword = oldPassword;
    
}
#pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.errorLabel.text = @"";
}
@end


@implementation UIView (Extention)
+ (UIView*) makeViewFromXib:(NSString*)xibName {
//    NSArray *allFrameworks = [NSBundle allFrameworks];
//    NSLog(@"allFrameworks:%@", allFrameworks);
//    NSArray *allBundles = [NSBundle allBundles];
//    NSLog(@"allBundles:%@", allBundles);
    
    NSArray* nibContents = [[NSBundle mainBundle] loadNibNamed:xibName owner:self options:nil];
    UIView *view = [nibContents lastObject];
    return view;
}
@end



